import actions.CalculateManhattanDistanceAction;
import model.Point;
import org.junit.Test;

public class ManhattanDistanceTest {
    Point p1 ;
    Point p2 ;
    int distance;

    @Test
    public void  calculate_manhattan_distance(){
        given_two_points();
        when_we_calculate_the_manhattan_distace();
        then_we_get_the_manhattan_distance();
    }
    //In a plane with p1 at (x1, y1) and p2 at (x2, y2), it is |x1 - x2| + |y1 - y2|.
    private void given_two_points() {
         p1 = new Point(3,5);
         p2 = new Point(2,6);
    }

    private void when_we_calculate_the_manhattan_distace() {
        CalculateManhattanDistanceAction calculateManhattanDistanceAction = new CalculateManhattanDistanceAction();
        distance =calculateManhattanDistanceAction.execute(p1,p2);
    }

    private void then_we_get_the_manhattan_distance() {
    }

}
